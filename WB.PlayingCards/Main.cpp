
// Playing Cards
// Wyatt Baughman

#include <iostream>
#include <conio.h>

using namespace std;

enum Suit
{
	Hearts,
	Spades,
	Diamonds,
	Clubs
};

enum Rank
{
	Two = 2,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	Ace
};

struct Card
{
	Suit suit = Suit::Hearts;
	Rank rank = Rank::Two;
	
};
void PrintCard(const Card card) {
	switch (card.rank) {
	case 2:
		cout << "the Two";
		break;
	case 3:
		cout << "the Three";
		break;
	case 4:
		cout << "the Four";
		break;
	case 5:
		cout << "the Five";
		break;
	case 6:
		cout << "the Six";
		break;
	case 7:
		cout << "the Seven";
		break;
	case 8:
		cout << "the Eight";
		break;
	case 9:
		cout << "the Nine";
		break;
	case 10:
		cout << "the Ten";
		break;
	case 11:
		cout << "the Jack";
		break;
	case 12:
		cout << "the Queen";
		break;
	case 13:
		cout << "the King";
		break;
	case 14:
		cout << " the Ace";
		break;
	}
	switch (card.suit)
	{
	case 0:
		cout << " of Hearts\n";
		break;
	case 1:
		cout << " of Spades\n";
		break;
	case 2:
		cout << " of Diamonds\n";
		break;
	case 3:
		cout << " of Clubs\n";
		break;
	}
}
Card HighCard(Card card1, Card card2) {
	if (card1.rank > card2.rank) {
		return card1;
	}
	else if (card2.rank > card1.rank){
		return card2;
	}
	else {
		cout << "draw";
	}
}

int main()
{
	Card c1;
	c1.rank = Jack;
	c1.suit = Clubs;
	PrintCard(c1);
	Card c2;
	c2.rank = Seven;
	c2.suit = Hearts;
	PrintCard(c2);
	(void)_getch;
	return 0;
}

